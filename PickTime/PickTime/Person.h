//
//  Person.h
//  PickTime
//
//  Created by Do Ngoc Trinh on 2/21/16.
//  Copyright © 2016 Do Ngoc Trinh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject
@property(nonatomic,copy) NSString* imageUrl;
@property(nonatomic,copy) NSString* name;
@property(nonatomic,copy) NSString* age;
@property(nonatomic,copy) NSString* date;
@property(nonatomic,copy) NSString* time;
+(instancetype) PersonWithImageUrl:(NSString*)image
                           andName:(NSString*)name
                            andAge:(NSString*)age
                           andDate:(NSString*)date
                           andTime:(NSString*)time;
-(NSString*) printPerson;
@end
