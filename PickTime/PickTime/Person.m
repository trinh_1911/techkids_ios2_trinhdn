//
//  Person.m
//  PickTime
//
//  Created by Do Ngoc Trinh on 2/21/16.
//  Copyright © 2016 Do Ngoc Trinh. All rights reserved.
//

#import "Person.h"

@implementation Person
+(instancetype) PersonWithImageUrl:(NSString*)image
                           andName:(NSString*)name
                            andAge:(NSString*)age
                           andDate:(NSString*) date
                           andTime:(NSString*)time;
{
    Person  *newPerson = [[Person alloc]init];
    newPerson.imageUrl = image;
    newPerson.name = name;
    newPerson.age = age;
    newPerson.date = date;
    newPerson.time = time;
    return newPerson;
}
-(NSString*) printPerson{
    return [NSString stringWithFormat:@"-------------------------\nName: %@\nAge: %@\nDatepicked: %@\nTimer: %@\nTmg: %@ \n",self.name, self.age, self.date, self.time, self.imageUrl];
}

@end
