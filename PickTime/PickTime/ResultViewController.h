//
//  ResultViewController.h
//  PickTime
//
//  Created by Do Ngoc Trinh on 2/21/16.
//  Copyright © 2016 Do Ngoc Trinh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResultViewController : UIViewController<UITableViewDataSource,UITabBarDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tblResultPicker;
@property (nonatomic, strong)NSArray* personList;

@end
