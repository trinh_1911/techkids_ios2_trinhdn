//
//  CustomCell.m
//  PickTime
//
//  Created by Do Ngoc Trinh on 2/26/16.
//  Copyright © 2016 Do Ngoc Trinh. All rights reserved.
//

#import "CustomCell.h"

@implementation CustomCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
