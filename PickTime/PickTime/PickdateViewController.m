//
//  PickdateViewController.m
//  PickTime
//
//  Created by Do Ngoc Trinh on 2/21/16.
//  Copyright © 2016 Do Ngoc Trinh. All rights reserved.
//

#import "PickdateViewController.h"
#import "Person.h"
#import "AppDelegate.h"
@interface PickdateViewController ()

{
    NSArray* imgArr;
    int currentImg;
    NSTimer* timer;
    NSDate* startDate;
    NSTimeInterval secondRun;
    BOOL isTimeRun;
    AppDelegate *ad;
    NSMutableArray *listPersons;
    //	NSInteger startTime;
    
}
@end




@implementation PickdateViewController

UIAlertView *alertView;
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    ad = [[UIApplication sharedApplication]delegate];
      
    listPersons = [[NSMutableArray alloc]init];
    
   //-----------------------------------------------------------
    [self resetTimer];
    
        imgArr = [NSArray arrayWithObjects:@"danong.jpd",@"deptrai.jpg",@"docthan.jpg", nil];
    currentImg = 0;
    [self.imgAva setImage: [UIImage imageNamed:[imgArr objectAtIndex:currentImg]]];
    
    //config tap to dimiss the keyboard with dis func over bottom v v v
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

//
////config tap to dimiss the keyboard with dis func over bottom v v v
//UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
//                               initWithTarget:self
//                               action:@selector(dismissKeyboard)];
//
//[self.view addGestureRecognizer:tap];

//
//-(void)dismissKeyboard {
//    [self.tfName resignFirstResponder];
//    [self.tfAge resignFirstResponder];
//    
//}


//touchesBegan


-(void)startTimer{
        startDate = [NSDate date];
        timer = [NSTimer scheduledTimerWithTimeInterval:24/60 target:self selector:@selector(timeTick:) userInfo:nil repeats:YES];
           [timer fire];
}

-(void)stopTimer{
    [timer invalidate];
    timer = nil;
}

-(void)timeTick:(id)sender{
    if(!isTimeRun) return;
    
    NSDate *currentDate =[NSDate date];
    
    NSTimeInterval timeInterval  = [currentDate timeIntervalSinceDate:startDate];
    timeInterval += secondRun;
    
    NSDate *timerDate = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    
    NSDateFormatter *dateFomatter = [[NSDateFormatter alloc]init];
    [dateFomatter setDateFormat:@"HH:mm:ss SS"];
    [dateFomatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0.0]];
    
    NSString *time = [dateFomatter stringFromDate:timerDate];
    
    //caculate time
//    NSInteger currentTime = [[NSDate date]timeIntervalSince1970];
//    NSInteger playedTime = currentTime - startTime;
//    
//    NSDate* currentDate = [NSDate dateWithTimeIntervalSince1970:playedTime];
//    
//    NSDateFormatter *df = [[NSDateFormatter alloc]init];
//    [df setDateFormat:@"HH:mm:ss SS"];
//    [df setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0.0]];
//    NSString* time = [df stringFromDate:currentDate];
   // NSLog(@"%@\n",time);
    self.lblTimer.text = time;
}

-(void)showArlert{
    
    NSString *alertTitle = @"Confirm";
    NSString *alertMessage = @"Do you wanna SAVE?";
    NSString *alertOkButtonText = @"OK";
    alertView = [[UIAlertView alloc] initWithTitle:alertTitle
                                           message:alertMessage
                                          delegate:self
                                 cancelButtonTitle:@"Cancel"
                                 otherButtonTitles:alertOkButtonText, nil];
    
        [alertView show];
}

#pragma mark - Alert view delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:
(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case 0:
            NSLog(@"Cancel button clicked");
            break;
        case 1:
            NSLog(@"OK button clicked");
            //so SAVE PERSON at HERE!
            [self savePerson];
            break;
            
        default:
            break;
    }
}
-(IBAction)btnChooseDidTap:(id)sender{
    int randomIndex = arc4random() % [imgArr count];
    //NSLog(@"count  %ld  %d",[imgArr count],randomIndex);
    currentImg = randomIndex;
    [self.imgAva setImage: [UIImage imageNamed:[imgArr objectAtIndex:randomIndex]]];
}


-(IBAction)btnSaveDidTap:(id)sender{

    
    [self stopTimer];

    [self showArlert];

}

-(void)savePerson;{
    NSString * image = @"";
    NSString * name = @"";
    NSString *age = @"";
    NSString* date =@"";
    NSString* timerText =@"";
    
    image =[imgArr objectAtIndex:currentImg];
    name = self.tfName.text;
    age = self.tfAge.text;
    date = [NSDateFormatter localizedStringFromDate:[_pickerDate date] dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterFullStyle];
    timerText = self.lblTimer.text;
    
    Person* newPerson = [Person PersonWithImageUrl:image andName:name andAge:age andDate:date andTime:timerText];
    
    // NSLog(@"%@",[newPerson printPerson]);
    [listPersons addObject:newPerson];
    
    //NSLog(@"Count: %ld",[listPersons count]);
    
    ad.listPersons = listPersons;
    
   // NSMutableArray *ii = ad.listPersons;
    // NSLog(@"II: %ld",[ii count]);
}
-(IBAction)btnPasueDidTap:(id )sender{
    if(!isTimeRun){
        isTimeRun = true;
        startDate = [NSDate date];
        [sender setTitle:@"Pause" forState:UIControlStateNormal];
        //startTime = [[NSDate date]timeIntervalSince1970];
        [self startTimer];
    }
    else{
        secondRun +=[[NSDate date] timeIntervalSinceDate:startDate];
        [sender setTitle:@"Resume" forState:UIControlStateNormal];
        isTimeRun = false;
        NSLog(@"time will resume at : %f",secondRun);
    }
    
    
}
-(void)resetTimer{
    [self stopTimer];
    secondRun = 0;
    self.lblTimer.text =@"00:00:00 00";
    [self.btnPause setTitle:@"Start" forState:UIControlStateNormal];
    isTimeRun = false;
}
-(IBAction)btnResetDidTap:(id)sender{
    [self resetTimer];
   }

//action for dismiss keyboard (to do them, we need UITapGesture in viewDidLoad above ^ ^ ^)
- (IBAction)tfNameDimiss:(id)sender {
    [self.tfName resignFirstResponder];
}

-(void)dismissKeyboard {
    [self.tfName resignFirstResponder];
    [self.tfAge resignFirstResponder];
    
}

@end
