//
//  ResultViewController.m
//  PickTime
//
//  Created by Do Ngoc Trinh on 2/21/16.
//  Copyright © 2016 Do Ngoc Trinh. All rights reserved.
//

#import "ResultViewController.h"
#import "Person.h"
#import "CustomCell.h"
#import "AppDelegate.h"

@interface ResultViewController ()
{
    AppDelegate *ad ;
   
}

@end


@implementation ResultViewController

- (void)viewDidLoad {

    [super viewDidLoad];
    //customcell
    [self.tblResultPicker registerNib:[UINib nibWithNibName:@"CustomCell" bundle:nil] forCellReuseIdentifier:@"cellID"];
      ad = [[UIApplication sharedApplication]delegate];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated;
{
    [super viewWillAppear:animated];
    [self.tblResultPicker reloadData];
    
    self.personList = ad.listPersons;
    NSLog(@"COUNT: %ld",[ad.listPersons count]);
    
    //Person *person1 = [Person PersonWithImageUrl:@"danong.jpg" andName:@"trinh" andAge:@"21" andDate:@"" andTime:@"00:00:00 99"];
    //self.personList = [NSArray arrayWithObjects:person1, nil];
}

//config tableView
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;{
  
    return [self.personList count];
}

-(UITableViewCell *)tableView: (UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath;
{
    static NSString *cellID = @"cellID";
    CustomCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if(cell==nil){
        cell = [[CustomCell alloc]initWithStyle:UITableViewCellStyleDefault
                                reuseIdentifier:cellID];
    }
    //create Person for cell
    
    Person *displayPerson = [self.personList objectAtIndex:indexPath.row];
    cell.lblName.text = displayPerson.name;
    cell.lblAge.text = displayPerson.age;
    cell.lblTimer.text = displayPerson.time;
    cell.lblDate.text = displayPerson.date;
    cell.imgAva.image = [UIImage imageNamed:displayPerson.imageUrl];
    
    
    return cell;
    
}
@end
