//
//  ClassViewController.h
//  baitap
//
//  Created by Do Ngoc Trinh on 1/28/16.
//  Copyright © 2016 Do Ngoc Trinh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Student.h"
@interface ClassViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tblStudent;
@property (nonatomic,strong) NSArray* student;
@end
