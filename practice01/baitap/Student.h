//
//  Student.h
//  baitap
//
//  Created by Do Ngoc Trinh on 1/28/16.
//  Copyright © 2016 Do Ngoc Trinh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Student : NSObject

@property(nonatomic,copy) NSString* name;
@property(nonatomic,assign) NSInteger age;
@property(nonatomic,copy) NSString* image;
@property(nonatomic,copy) NSString* gender;
@property(nonatomic,copy) NSString* contact;
@property(nonatomic,copy) NSString* address;
+(instancetype) StudentWithName:(NSString*)name
                         andAge:(NSInteger)age
andImage:(NSString*)image
                      andGender:(NSString*)gender
                     andContact:(NSString*)contact
                     andAddress:(NSString*)address;
@end
