//
//  ClassViewController.m
//  baitap
//
//  Created by Do Ngoc Trinh on 1/28/16.
//  Copyright © 2016 Do Ngoc Trinh. All rights reserved.
//

#import "ClassViewController.h"
#import "Student.h"
#import "CustomCell.h"
#import "DetailStudentController.h"

@interface ClassViewController ()

@end

@implementation ClassViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title=@"Student List";
    
    [self.tblStudent registerNib:[UINib nibWithNibName:@"CustomCell" bundle:nil] forCellReuseIdentifier:@"cellID"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated;
{
    [super viewWillAppear:animated];
    Student *trinh = [Student StudentWithName:@"Do Ngoc Trinh" andAge:20 andImage:@"Trinh.png" andGender:@"Male" andContact:@"0000-000-000" andAddress:@"Megatron"];
    Student *obama = [Student StudentWithName:@"Nguyen Van Obama" andAge:20 andImage:@"Obama.png" andGender:@"Male" andContact:@"0123-999-000" andAddress:@"Moon"];
    self.student = [NSArray arrayWithObjects:trinh , obama, nil];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    static NSString *cellID = @"cellID";
    CustomCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if(cell ==nil){
        cell =[[CustomCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    Student *displayStudent = [self.student objectAtIndex:indexPath.row];
    cell.lblName.text = displayStudent.name;
    //cell.lblAge.text = [NSString stringWithFormat:@"%d",displayStudent.age];
    cell.imgAvatar.image = [UIImage imageNamed:displayStudent.image];
    return cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{ 
    return [self.student count];
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath;{
    Student *selectedStudent = [self.student objectAtIndex:indexPath.row];
    DetailStudentController *detailStudentController = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailStudentController"];
    //[self presentViewController:detailStudentController animated:YES completion:nil];
    [self.navigationController pushViewController:detailStudentController animated:YES];
    
    detailStudentController.selectedStudent = selectedStudent;
//    NSLog(@"%@ %@",detailStudentController.selectedStudent.name,selectedStudent.name);
//  detailStudentController.lblName.text = detailStudentController.selectedStudent.name;
//    detailStudentController.lblAge.text = detailStudentController.selectedStudent.name;
//    detailStudentController.imgAvatar.image =	[UIImage imageNamed: selectedStudent.image];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
 
*/
//-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender;
//{
//    /*
//     //if needed, we need to check if segue destinationcontroller is exactly what we need right now, in this case, StudentDetail view
//     */
//     if ([segue.identifier isEqualToString:@"studentDetail"]) {
//     //do passing data here
//    DetailStudentController *detailViewController = segue.destinationViewController;
//    NSIndexPath *selectedIndexPath =[self.tblStudent indexPathForSelectedRow];
//    detailViewController.selectedStudent = [self.student objectAtIndex:selectedIndexPath.row];
//     }
//    
//    
//}

@end
