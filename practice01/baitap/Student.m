//
//  Student.m
//  baitap
//
//  Created by Do Ngoc Trinh on 1/28/16.
//  Copyright © 2016 Do Ngoc Trinh. All rights reserved.
//

#import "Student.h"

@implementation Student

+(instancetype) StudentWithName:(NSString*)name
                         andAge:(NSInteger)age
                         andImage:(NSString*)image
andGender:(NSString *)gender andContact:(NSString *)contact andAddress:(NSString *)address;
{
    Student *newStudent = [[Student alloc]init];
    newStudent.name = name;
    newStudent.age = age;
    newStudent.image = image;
    newStudent.gender = gender;
    newStudent.contact = contact;
    newStudent.address = address;
    
    return newStudent;
}

@end
