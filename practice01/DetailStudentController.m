//
//  DetailStudentController.m
//  baitap
//
//  Created by Do Ngoc Trinh on 1/29/16.
//  Copyright © 2016 Do Ngoc Trinh. All rights reserved.
//

#import "DetailStudentController.h"

@interface DetailStudentController ()

@end

@implementation DetailStudentController

- (void)viewDidLoad {
    [super viewDidLoad];
    //NSLog(@"saaa %@",self.lblName);
    self.lblName.text = self.selectedStudent.name;
    self.lblAge.text = [NSString stringWithFormat:@"%ld",self.selectedStudent.age];
    self.imgAvatar.image = [UIImage imageNamed:self.selectedStudent.image];
    self.lblGender.text = self.selectedStudent.gender;
    self.lblContact.text = self.selectedStudent.contact;
    self.lblAddress.text = self.selectedStudent.address;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
