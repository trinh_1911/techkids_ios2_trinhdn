//
//  DetailStudentController.h
//  baitap
//
//  Created by Do Ngoc Trinh on 1/29/16.
//  Copyright © 2016 Do Ngoc Trinh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Student.h"

@interface DetailStudentController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *lblGender;
@property (weak, nonatomic) IBOutlet UILabel *lblContact;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UIImageView *imgAvatar;
@property (weak, nonatomic) IBOutlet UILabel *lblAge;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
//@property (weak, nonatomic) IBOutlet UILabel *lblDetailName;
//@property (weak, nonatomic) IBOutlet UILabel *lblDetailAge;
//@property (weak, nonatomic) IBOutlet UIImageView *imgDetail;
@property (nonatomic,assign) Student *selectedStudent;

@end
