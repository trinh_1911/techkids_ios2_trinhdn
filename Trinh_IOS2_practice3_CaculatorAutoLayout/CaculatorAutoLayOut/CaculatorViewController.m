//
//  CaculatorViewController.m
//  CaculatorAutoLayOut
//
//  Created by Do Ngoc Trinh on 3/17/16.
//  Copyright © 2016 Do Ngoc Trinh. All rights reserved.
//

#import "CaculatorViewController.h"
#import "Masonry.h"
@interface CaculatorViewController ()

@end

@implementation CaculatorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    UIView *superview = self.view;
    [self.view removeConstraints:self.view.constraints ];
    
    
    [self.viewKQ mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superview.mas_top);
        make.leading.equalTo(superview.mas_leading);
        make.height.equalTo(superview.mas_height).multipliedBy(1.0f/5.0f);
        make.width.equalTo(superview.mas_width);
    }];
    
    [self.viewPAD mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.viewKQ.mas_bottom);
        make.leading.equalTo(superview.mas_leading);
        make.width.equalTo(superview.mas_width);
        make.bottom.equalTo(self.view.mas_bottom);
        
    }];
 }
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
