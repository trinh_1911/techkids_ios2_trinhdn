//
//  AppDelegate.h
//  CaculatorAutoLayOut
//
//  Created by Do Ngoc Trinh on 3/16/16.
//  Copyright © 2016 Do Ngoc Trinh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

