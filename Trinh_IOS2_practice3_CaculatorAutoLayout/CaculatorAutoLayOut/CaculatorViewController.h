//
//  CaculatorViewController.h
//  CaculatorAutoLayOut
//
//  Created by Do Ngoc Trinh on 3/17/16.
//  Copyright © 2016 Do Ngoc Trinh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CaculatorViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *viewKQ;
@property (weak, nonatomic) IBOutlet UILabel *lblKQ;
@property (weak, nonatomic) IBOutlet UIView *viewPAD;

@property (weak, nonatomic) IBOutlet UIButton *btnAC;
@property (weak, nonatomic) IBOutlet UIButton *btnPlusMinus;
@property (weak, nonatomic) IBOutlet UIButton *btn7;
@property (weak, nonatomic) IBOutlet UIButton *btn4;
@property (weak, nonatomic) IBOutlet UIButton *btn1;
@property (weak, nonatomic) IBOutlet UIButton *btn0;
@property (weak, nonatomic) IBOutlet UIButton *btn8;
@property (weak, nonatomic) IBOutlet UIButton *btn5;
@property (weak, nonatomic) IBOutlet UIButton *btn2;
@property (weak, nonatomic) IBOutlet UIButton *btnPercent;
@property (weak, nonatomic) IBOutlet UIButton *btn9;
@property (weak, nonatomic) IBOutlet UIButton *btn6;
@property (weak, nonatomic) IBOutlet UIButton *btn3;
@property (weak, nonatomic) IBOutlet UIButton *btnComma;
@property (weak, nonatomic) IBOutlet UIButton *btnEqual;
@property (weak, nonatomic) IBOutlet UIButton *btnPlus;
@property (weak, nonatomic) IBOutlet UIButton *btnMinus;
@property (weak, nonatomic) IBOutlet UIButton *btnMulti;
@property (weak, nonatomic) IBOutlet UIButton *btnDiv;

@end
