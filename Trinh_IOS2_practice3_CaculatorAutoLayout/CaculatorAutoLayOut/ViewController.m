//
//  ViewController.m
//  CaculatorAutoLayOut
//
//  Created by Do Ngoc Trinh on 3/16/16.
//  Copyright © 2016 Do Ngoc Trinh. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _lblResult.numberOfLines = 1;

    CGFloat height = _lblResult.bounds.size.height;
    NSLog(@"%lf",height);
    
    UIFontDescriptor *descript  = [[UIFontDescriptor alloc]initWithFontAttributes:@{UIFontDescriptorFamilyAttribute: @"Helvetica Neue",
                                                                            UIFontDescriptorFaceAttribute: @"Thin"}];
    UIFont *font = [UIFont fontWithDescriptor:descript size:height*11/16];
   _lblResult.font = font;

 // 've not completed yet
}

-(void)btnDidTap:(UIButton *)sender{
    NSLog(@"\nIN %@",sender.titleLabel.text);
    [sender setAlpha:.5];
}

- (void)btnRelease:(UIButton *)sender {
   NSLog(@"\nOUT %@",sender.titleLabel.text);
    [sender setAlpha:1];
}


@end
