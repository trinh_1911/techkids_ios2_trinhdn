//
//  Suggestion.m
//  SearchDemo
//
//  Created by Do Ngoc Trinh on 3/20/16.
//  Copyright © 2016 Do Ngoc Trinh. All rights reserved.
//

#import "Suggestion.h"

@implementation Suggestion
-(instancetype)initWithJson:(NSDictionary *)jsonDict;
{
    self = [super init];
    if(self)
    {
        _query = jsonDict[@"query"];
        _query = [_query stringByReplacingOccurrencesOfString:@"<b>" withString:@""];
        _query = [_query stringByReplacingOccurrencesOfString:@"</b>" withString:@""];
       // NSLog(@"\n\n-----query %@",_query);
        _kind = jsonDict[@"kind"];
        if([_kind  isEqual: @"user"]){
           // NSLog(@"\n\n\nANH CA NHAN");
        }
        else{
           // NSLog(@"\n\n\nTRACK");
        }
        _trackId = jsonDict[@"id"];
        _score = jsonDict[@"score"];
    }
    return self;
}
@end
