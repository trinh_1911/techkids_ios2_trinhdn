//
//  CustomCell.h
//  SearchDemo
//
//  Created by Do Ngoc Trinh on 3/20/16.
//  Copyright © 2016 Do Ngoc Trinh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCell : UITableViewCell
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgView;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end
