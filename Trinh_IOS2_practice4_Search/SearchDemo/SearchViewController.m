//
//  SearchViewController.m
//  SearchDemo
//
//  Created by Do Ngoc Trinh on 3/20/16.
//  Copyright © 2016 Do Ngoc Trinh. All rights reserved.
//

#import "SearchViewController.h"
#import "SVPullToRefresh.h"
#import "AFNetworking/AFNetworking.h"
#import "UIImageView+WebCache.h"
#import "Constant.h"
#import "Suggestion.h"
#import "CustomCell.h"
#import "Track.h"

@interface SearchViewController ()
@property(nonatomic,strong)NSMutableArray *suggestions;
@property(nonatomic, strong) NSMutableArray *tracks;
@property int offset;
@property int suggest_offset;
@property(nonatomic,strong)NSString *currentSearch;
@property(nonatomic,strong) UIRefreshControl *refreshControl;
@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _offset = 0;
    _suggest_offset = 0;
    _currentSearch = @"";
    _searchBar.delegate = self;
    _suggestions = [[NSMutableArray alloc]init];
    _tracks = [[NSMutableArray alloc]init];
   
    _tbvSuggesttion.hidden = YES;
    
    //setup load more
    [_tbvSearchedTrack addInfiniteScrollingWithActionHandler:^{
        [self insertRowAtBottom];
    }];
    
    //setup pull torefresh
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.tbvSearchedTrack addSubview:refreshControl];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    }

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self dimissKeyboard];
     [_searchBar setShowsCancelButton:NO animated:YES];
}
-(void)dimissKeyboard;{
    [_searchBar resignFirstResponder];
}


#pragma mark - PULL TO REFRESH & INFINITE SCROLLING
- (void)refresh:(UIRefreshControl *)refreshControl {
    NSLog(@"Refresh to load");
    _offset =0;
    [_tracks removeAllObjects];
    [_tbvSearchedTrack reloadData];
    
    [self setCurrentSeach];
    
    [self.tbvSearchedTrack.infiniteScrollingView stopAnimating];
    [refreshControl endRefreshing];
}

-(void)insertRowAtBottom{
    
       _offset += 25;
  //  NSLog(@"I'm loading more hihi :) ::%@ %d::",_currentSearch,_offset);

    if(_tracks.count>0){
        [self setCurrentSeach];
    }
    [self.tbvSearchedTrack.infiniteScrollingView stopAnimating];
}


#pragma mark - Networking
- (void)autoCompleteWithKeyWord:(NSString *)keyword andOffset:(int)offset completionBlock:(void(^)(NSArray *tracks))completion;
{
    //keyword = [keyword stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
    keyword = [keyword stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLUserAllowedCharacterSet]];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFHTTPSessionManager *httpSessionManager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:configuration];
    
    NSString *stringUrl =  [NSString stringWithFormat:kSoundCloudAutoCompleteUrl,keyword,_suggest_offset];
    
    [httpSessionManager GET:stringUrl parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if (completion&&responseObject) {
            completion(responseObject[@"suggestions"]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"\n\n\n\nYou've the ERROR: %@",error);
    }];
}
-(void)searchTracksWithKeyword:(NSString *)keyword offset:(int)offset ;{
    
     keyword = [keyword stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLUserAllowedCharacterSet]];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFHTTPSessionManager *httpSessionManager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:configuration];
    NSString *stringUrl =  [NSString stringWithFormat:kSoundCloudSearchTrackUrl,keyword,offset];

    [httpSessionManager GET:stringUrl parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {

        for (NSDictionary *jsonDict in responseObject) {
           
            Track *newTrack = [[Track alloc]initWithJson:jsonDict];
            
            [_tracks addObject:newTrack];
        }
        [_tbvSearchedTrack reloadData];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"\n\n\n\nYou've the ERROR: %@",error);
    }];
}

#pragma mark - Search Bar
-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    
    [_searchBar setShowsCancelButton:YES animated:YES];
    
    if (_searchBar.text.length > 0) {
        _tbvSuggesttion.hidden = NO;
        
    }
}
//-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
//    //[_searchBar resignFirstResponder];
//    [_searchBar setShowsCancelButton:NO animated:YES];
//    //[_tbvSuggesttion setHidden:YES];
//
//}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
   
    if (_searchBar.text.length > 0) {
        _tbvSuggesttion.hidden = NO;
    } else {
        _tbvSuggesttion.hidden = YES;
    }
    
    NSString *suggestionString = _searchBar.text;
    [self autoCompleteWithKeyWord: suggestionString andOffset:_suggest_offset completionBlock:^(NSArray *suggestions) {
        NSMutableArray *tmpSuggestion = [[NSMutableArray alloc]init];
        
        for (NSDictionary *jsonDict in suggestions) {
            Suggestion *newSuggestion = [[Suggestion alloc]initWithJson:jsonDict];
            [tmpSuggestion addObject:newSuggestion];
        }
        _suggestions = tmpSuggestion;
        [_tbvSuggesttion reloadData];

    }];
    
}
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
     [_searchBar setShowsCancelButton:NO animated:YES];
    _tbvSuggesttion.hidden = YES;
     _tbvSuggesttion.hidden = YES;
    
}
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [_searchBar resignFirstResponder];
    _tbvSuggesttion.hidden = YES;
    
    [_tracks removeAllObjects];
    [_tbvSearchedTrack reloadData];
    
    [self setCurrentSeach];
}

#pragma mark - TableDatasource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == _tbvSuggesttion) {
        return _suggestions.count;
    }
    else{
        return _tracks.count;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == _tbvSuggesttion){
        CustomCell *cell  = [[CustomCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SuggestCell"];
        cell.textLabel.text = [_suggestions[indexPath.row] query];
        
        cell.imageView.image = [UIImage imageNamed:@"defaultIMG"];
        if([[_suggestions[indexPath.row] kind] isEqualToString:@"user"]){
            cell.imageView.image = [UIImage imageNamed:@"userIMG"];
        }
        else if ([[_suggestions[indexPath.row] kind] isEqualToString:@"track"]){
             cell.imageView.image = [UIImage imageNamed:@"trackIMG"];
        }
        return cell;
    }
    else{
        CustomCell *cell  = [[CustomCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TrackCellID"];
        
        Track *track = _tracks[indexPath.row];
        cell.textLabel.text =[track title];
        
        UIImage *imgPlaceholder = [UIImage imageNamed:@"imgPlaceHolder"];
        if([track.artworkURL isEqualToString:@""]){
            cell.imageView.image = imgPlaceholder;
        }
        else{
            [cell.imageView sd_setImageWithURL:[NSURL URLWithString:track.artworkURL] placeholderImage:imgPlaceholder];
        }
        
        return cell;
    }
    
}

#pragma mark - TableView delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (tableView == _tbvSuggesttion) {
        
        _searchBar.text = [_suggestions[indexPath.row] query];
        [_searchBar endEditing:YES];
       
        [_tracks removeAllObjects];
        [_tbvSearchedTrack reloadData];
        //search
        [self setCurrentSeach];
        
        _tbvSuggesttion.hidden = YES;
    }
    else{
        
    }
}
-(void)setCurrentSeach;{
    NSString *stringUrl = _searchBar.text;
    //Unicode accept
    //NSString *stringUTF8 = [stringUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLUserAllowedCharacterSet]];
    _currentSearch = stringUrl;//stringUTF8;
    
    if(![_currentSearch isEqualToString:@""]){
        [self searchTracksWithKeyword:_currentSearch offset:_offset];
    }
}
@end
