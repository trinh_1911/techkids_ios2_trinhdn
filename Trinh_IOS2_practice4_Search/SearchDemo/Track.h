//
//  Track.h
//  DemoSearchSoundCoud
//
//  Created by Trung Đức on 3/19/16.
//  Copyright © 2016 Trung Đức. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Track : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *trackId;
@property(nonatomic,strong)NSString *artworkURL;

- (instancetype)initWithJson:(NSDictionary *)jsonDict;

@end
