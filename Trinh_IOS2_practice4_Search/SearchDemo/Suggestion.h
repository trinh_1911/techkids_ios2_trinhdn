//
//  Suggestion.h
//  SearchDemo
//
//  Created by Do Ngoc Trinh on 3/20/16.
//  Copyright © 2016 Do Ngoc Trinh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Suggestion : NSObject
@property (nonatomic, strong) NSString *query;
@property (nonatomic, strong) NSString *kind;
@property (nonatomic, strong) NSString *trackId;
@property (nonatomic, strong) NSString *score;

- (instancetype)initWithJson:(NSDictionary *)jsonDict;

@end
