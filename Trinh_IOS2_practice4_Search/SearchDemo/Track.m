//
//  Track.m
//  DemoSearchSoundCoud
//
//  Created by Trung Đức on 3/19/16.
//  Copyright © 2016 Trung Đức. All rights reserved.
//

#import "Track.h"

@implementation Track

- (instancetype)initWithJson:(NSDictionary *)jsonDict;
{
    self = [super init];
    
    if (self) {
        _title = jsonDict[@"title"];
        _trackId = jsonDict[@"id"];
        if ([jsonDict[@"artwork_url"] isKindOfClass:[NSNull class]]) {
            self.artworkURL = @"";
        } else {
            self.artworkURL = jsonDict[@"artwork_url"];
        }
    }
    
    return self;
}


@end
