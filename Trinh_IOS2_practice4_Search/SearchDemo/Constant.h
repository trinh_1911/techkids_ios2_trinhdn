//
//  Constant.h
//  SearchDemo
//
//  Created by Do Ngoc Trinh on 3/20/16.
//  Copyright © 2016 Do Ngoc Trinh. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kSoundCloudAutoCompleteUrl              @"https://api.soundcloud.com/search/suggest?client_id=a453dc1eaf606c2c7c95e263e0c4f385&q=%@&offset=%d&limit=15"

#define kSoundCloudSearchTrackUrl               @"https://api.soundcloud.com/tracks.json?client_id=a453dc1eaf606c2c7c95e263e0c4f385&q=%@&offset=%d&limit=25"


