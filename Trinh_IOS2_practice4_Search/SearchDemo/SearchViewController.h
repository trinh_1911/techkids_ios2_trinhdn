//
//  SearchViewController.h
//  SearchDemo
//
//  Created by Do Ngoc Trinh on 3/20/16.
//  Copyright © 2016 Do Ngoc Trinh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchViewController : UIViewController<UISearchBarDelegate,UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *tbvSuggesttion;
@property(weak, nonatomic)IBOutlet UITableView *tbvSearchedTrack;
@end
